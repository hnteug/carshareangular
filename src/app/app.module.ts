import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
// import { MembreComponent } from './account/membre/membre.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MembreCreateComponent } from './components/membre-create/membre-create.component';
import { CentreFormationComponent } from './components/centre-formation/centre-formation.component';
import { CentreformartionListComponent } from './components/centreformartion-list/centreformartion-list.component';
import { MenuComponent } from './menu/menu/menu.component';
import { LoginComponent } from './login/login/login.component';
import { OffreCovoiturageComponent } from './components/offre-covoiturage/offre-covoiturage.component';
import { SearchComponent } from './components/search/search.component';
import { MembreUpdateComponent } from './components/membre-update/membre-update.component';
import { VehiculeCreateComponent } from './components/vehicule-create/vehicule-create.component';
import { MembreListComponent } from './components/membre-list/membre-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MembreCreateComponent,
    CentreFormationComponent,
    CentreformartionListComponent,
    MenuComponent,
    LoginComponent,
    OffreCovoiturageComponent,
    SearchComponent,
    MembreUpdateComponent,
    VehiculeCreateComponent,
    MembreListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
