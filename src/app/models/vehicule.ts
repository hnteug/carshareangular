import { Ivehicule } from "../interfaces/ivehicule";
import { Membre } from "./membre";

export class Vehicule implements Ivehicule {

  public  idVehicule: number;
  public  numeroPlaque: string;
  public  marque: string;
  public  modele: string;
  public  membre: Membre;
}
