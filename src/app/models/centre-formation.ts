import { IcentreFormation } from "../interfaces/icentre-formation";

export class CentreFormation implements IcentreFormation {
  public  idCentre: number;
  public   nomCentre: string;
}
