import { IoffreCovoiturage } from "../interfaces/ioffre-covoiturage";
import { Membre } from "./membre";

export class OffreCovoiturage  implements IoffreCovoiturage  {
  public  idOffre: number;
  public nombrePlace:number;
  public dateCreation: Date
  public   fumeur: boolean;
  public   mange: boolean;
  public  music: boolean;
   public  membre: Membre;
}
