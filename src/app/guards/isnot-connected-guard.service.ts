import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { MembreService } from '../services/membre.service';

@Injectable({
  providedIn: 'root'
})
//cette methode permet que qd le membre
export class IsnotConnectedGuard implements CanActivate {

  constructor(private _mbreService:MembreService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this._mbreService.currentUser==null || this._mbreService.currentUser==undefined
  }
}
