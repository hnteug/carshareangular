import { TestBed } from '@angular/core/testing';

import { IsnotConnectedGuardService } from './isnot-connected-guard.service';

describe('IsnotConnectedGuardService', () => {
  let service: IsnotConnectedGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IsnotConnectedGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
