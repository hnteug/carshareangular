import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { MembreService } from '../services/membre.service';

@Injectable({
  providedIn: 'root'
})
export class IsConnectedGuard implements CanActivate{
// lire la doc sur les guard
  constructor(private _membreService:MembreService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
   // throw new Error('Method not implemented.');
   return this._membreService.currentUser!=null && this._membreService.currentUser !=undefined
  }
}
