import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginComponent } from '../login/login/login.component';

@Injectable({
  providedIn: 'root'
})

export class LeaveGuard implements CanDeactivate<LoginComponent> {
  constructor() { }
  canDeactivate(component: LoginComponent,
  currentRoute: ActivatedRouteSnapshot,
  currentState: RouterStateSnapshot,
  nextState?: RouterStateSnapshot): boolean {
  throw new Error("Method not implemented.");
  }
  }
