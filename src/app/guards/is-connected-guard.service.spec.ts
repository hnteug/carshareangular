import { TestBed } from '@angular/core/testing';

import { IsConnectedGuardService } from './is-connected-guard.service';

describe('IsConnectedGuardService', () => {
  let service: IsConnectedGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IsConnectedGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
