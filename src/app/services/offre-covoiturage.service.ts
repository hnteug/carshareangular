import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subscribable } from 'rxjs';
import { OffreCovoiturage } from '../models/offre-covoiturage';

@Injectable({
  providedIn: 'root'
})
export class OffreCovoiturageService {
  private url : string = "https://localhost:44374/api/"

  constructor(private _client : HttpClient) { }

  InsertOffreC(OffreC:OffreCovoiturage):Subscribable<number> {
    return this._client.post(this.url+"insererOffre/", OffreC);
   } 
}
