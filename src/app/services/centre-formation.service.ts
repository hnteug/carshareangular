import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subscribable } from 'rxjs';
import { CentreFormation } from '../models/centre-formation';
import {IcentreFormation} from '../interfaces/icentre-formation';

@Injectable({
  providedIn: 'root'
})
export class CentreFormationService {
  private url : string = "https://localhost:44374/api/"

  constructor(private _client : HttpClient ) { }

  InsertCentreF(CF:CentreFormation):Subscribable<number> {
    return this._client.post(this.url+"ajoutCf/", CF);
   }

   GetAllCentreF():Subscribable<IcentreFormation[]> {
    return this._client.get(this.url+"getAllCF/");
  }

/*   getCentreById(id): any {
    return this._client.get(this.url + "/" + id );
   }*/
}
