import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subscribable } from 'rxjs';
import { Vehicule } from '../models/vehicule';

@Injectable({
  providedIn: 'root'
})
export class VehiculeService {

  private url : string = "https://localhost:44374/api/"
  constructor(private _client : HttpClient ) { }

  InsertVh(Vh:Vehicule):Subscribable<number> {
    return this._client.post(this.url+"insererVehicule/", Vh);
   }
}
