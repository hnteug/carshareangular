import { TestBed } from '@angular/core/testing';

import { CentreFormationService } from './centre-formation.service';

describe('CentreFormationService', () => {
  let service: CentreFormationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CentreFormationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
