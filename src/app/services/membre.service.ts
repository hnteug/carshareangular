import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subscribable, Subscriber } from 'rxjs';
import { Router } from '@angular/router';
import { Membre } from '../models/membre';

@Injectable({
  providedIn: 'root'
})
export class MembreService {
// c'est l'url du projet api .net core
  private url : string = "https://localhost:44374/api/"
  private _currentUser:Membre;
  
  constructor(
         private _client : HttpClient   
  ) { }
//le service esr juste la pour faire la transition 
  InsertMbre(Mbre:Membre):Subscribable<number> {
 return this._client.post(this.url+"ajoutMbre/", Mbre);
}

getOneMbre(id : number) : Observable<Membre> {
  return this._client.get<Membre>(this.url+"getMbre/"+id)
}

checkpwd(login:string, Password:string):Subscribable<number>{
  return this._client.post<number>(this.url+"checkpwd/",{Login:login, Password:Password} )
}

get currentUser(): Membre {
  if (sessionStorage.getItem('user')) {
    this._currentUser = JSON.parse(sessionStorage.getItem('user'));
    return this._currentUser;
  }
}

userIsConnected(): boolean {
  if (this.currentUser !== undefined && this.currentUser !== null) {
    return this._currentUser.connecte;
  }
  return false;
}


UpdateMbre(Mbre:Membre): Subscribable<void> {
  return this._client.put<void>(this.url+"updatedataMbre/", Mbre);
 }

 GetAllMembre():Subscribable<Membre[]> {
  return this._client.get(this.url+"getAllMbre/");
}
}


