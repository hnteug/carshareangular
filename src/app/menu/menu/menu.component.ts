import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MembreService } from 'src/app/services/membre.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  
  constructor(private _route:Router ) { }

  ngOnInit(): void {
  }

  public logout():void{
    sessionStorage.clear()
    this._route.navigateByUrl("login")
  }
}
