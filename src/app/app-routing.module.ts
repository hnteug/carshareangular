import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CentreFormationComponent } from './components/centre-formation/centre-formation.component';
import { CentreformartionListComponent } from './components/centreformartion-list/centreformartion-list.component';
import { MembreCreateComponent } from './components/membre-create/membre-create.component';
import { MembreListComponent } from './components/membre-list/membre-list.component';
import { MembreUpdateComponent } from './components/membre-update/membre-update.component';
 import { OffreCovoiturageComponent } from './components/offre-covoiturage/offre-covoiturage.component'; 

import { SearchComponent } from './components/search/search.component';
import { VehiculeCreateComponent } from './components/vehicule-create/vehicule-create.component';
import { IsConnectedGuard } from './guards/is-connected-guard.service';
import { IsnotConnectedGuard } from './guards/isnot-connected-guard.service';
import { LeaveGuard } from './guards/leave-guard.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login/login.component';
//le nom du path doit correspondre au nom du routerLink dans la appcomponent.html
const routes: Routes = [

  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },

  {
    path: 'offre-covoiturage',
    component:OffreCovoiturageComponent,
    canActivate:[IsConnectedGuard]
  }, 

  {
    path: 'login',
    component: LoginComponent,
    canActivate:[IsnotConnectedGuard]
    //canDeactivate: [LeaveGuard]
  },
  {
    path: 'search',
    component: SearchComponent
  },
  { 
    path: 'membre-create',
    component:MembreCreateComponent
  },

  { 
    path: 'vehicule-create',
    component:VehiculeCreateComponent,
    canActivate:[IsConnectedGuard]

  },
  { 
    path: 'membre-update',
    component:MembreUpdateComponent
  },
  { 
    path: 'centre-formation', 
    component:CentreFormationComponent
  },
  { 
    path: 'centreformation-list',
    component:CentreformartionListComponent
  },
  { 
    path: 'membre-list',
    component:MembreListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


