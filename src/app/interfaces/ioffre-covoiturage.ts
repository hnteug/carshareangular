import { Membre } from "../models/membre";

export interface IoffreCovoiturage {
    idOffre:number
    nombrePlace:number;
    dateCreation: Date;
    fumeur :boolean
    mange :boolean
    music:boolean
    membre: Membre

}
