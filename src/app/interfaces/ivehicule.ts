import { Membre } from "../models/membre";

export interface Ivehicule {

    idVehicule:number
    numeroPlaque :string
    marque :string
    modele:string
    membre: Membre
}
