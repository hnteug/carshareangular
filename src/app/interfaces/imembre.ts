import { CentreFormation } from '../models/centre-formation';
import {Membre} from '../models/membre';

export interface Imembre {

         idMembre:number
         nom :string
         prenom :string
         email:string
          login :string
          password: string
           dateNaiss :Date
          dateValiditePermis: Date
          dateValiditeCF :Date
          dateFinCF: Date
         centreFormation:CentreFormation
  
        Insert(membre: Membre ): boolean;
}
