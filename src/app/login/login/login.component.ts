import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MembreService } from '../../services/membre.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  f: FormGroup;
  errorMsg = '';

  public get showIsConnected(): boolean {
    return this.membreService.userIsConnected();
  }
  constructor(private builder: FormBuilder,
    private router: Router,
    public membreService: MembreService) { }

  ngOnInit(): void {
    this.f = this.builder.group({
      pseudo_log: ['', [Validators.required]],
      pwd_log: ['', [Validators.required]]
    });
  }

  logout(): void {
    const user = this.membreService.currentUser;
    user.connecte = false;
    sessionStorage.setItem('user', JSON.stringify(user));

  }

  sign(): void {
   /* if (this.membreService.currentUser !== null && this.membreService.currentUser !=undefined) {
      this.membreService.checkpwd(this.membreService.currentUser.login, this.membreService.currentUser.password)
        .subscribe(
          (data) =>
            this.membreService.getOneMbre(data).subscribe(m => {

              sessionStorage.setItem('user', JSON.stringify(m));
              this.router.navigate(["vehicule-create"])
            },
              (error) => { throw new Error("check login et password") }),
          (error) => {
            sessionStorage.clear();
            console.error(error);
          });
    }
    else */if (this.membreService.currentUser == null && this.f.valid && !this.f.pristine) {
      this.membreService.checkpwd(this.f.controls['pseudo_log'].value, this.f.controls['pwd_log'].value)
      .subscribe(
      (data) =>
          this.membreService.getOneMbre(data).subscribe(m => {
            sessionStorage.setItem('user', JSON.stringify(m));
            this.router.navigate(["vehicule-create"])
          },
            (error) => { throw new Error("aucun membre ne correspond") }),
        (error) => {
          sessionStorage.clear();
          console.error(error);}
         
        );
      //le code plus haut permet d'enreg un user dans la session storage selon son id 
      // peut se mettre en place avec le JWT


    } else {
      console.error("vous n'etes pas loggé.")
    }

  }


}
