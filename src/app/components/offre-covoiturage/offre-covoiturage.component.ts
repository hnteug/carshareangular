import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Membre } from 'src/app/models/membre';
import { OffreCovoiturage } from 'src/app/models/offre-covoiturage';
import { OffreCovoiturageService } from '../../services/offre-covoiturage.service';

@Component({
  selector: 'app-offre-covoiturage',
  templateUrl: './offre-covoiturage.component.html',
  styleUrls: ['./offre-covoiturage.component.scss']
})
export class OffreCovoiturageComponent implements OnInit {
  fg : FormGroup
  constructor(private builder: FormBuilder,
    private _service : OffreCovoiturageService,
    private _router: Router) { }

  ngOnInit(): void {
    this.fg = this.builder.group({
      nbrePlace: ['', Validators.required],
      dCreation: ['', Validators.required],
      mg : ['', Validators.required],
      fum: ['', Validators.required],
      musiq:['', Validators.required], 
      mbre:[sessionStorage.getItem('userId'), Validators.required]
    })
  }


  InsertOffre(){
    if(this.fg.invalid){
      console.error("Formulaire invalide");
      return;
    }
    const newOffre = new OffreCovoiturage()
    let values = this.fg.value
    newOffre.nombrePlace=values["nbrePlace"]
    newOffre.dateCreation=values["dCreation"]
    newOffre.mange=values["mg"]
    newOffre.fumeur=values["fum"]
    newOffre.music=values["musiq"]
     newOffre.membre=new Membre()
     newOffre.membre.idMembre= values["mbre"]
 this._service.InsertOffreC(newOffre).subscribe({
      next : () => this._router.navigate(["/home"]),
      error : (error) => console.log(error)
    }) 
  }

}
