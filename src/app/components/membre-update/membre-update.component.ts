import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IcentreFormation } from '../../interfaces/icentre-formation';
import { CentreFormationService } from '../../services/centre-formation.service';
import { Membre } from '../../models/membre';
import { MembreService } from '../../services/membre.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-membre-update',
  templateUrl: './membre-update.component.html',
  styleUrls: ['./membre-update.component.scss']
})
export class MembreUpdateComponent implements OnInit {
  fg : FormGroup
  currentMbre : Membre
  centreFormationList:IcentreFormation[];
  constructor( private _service : MembreService,
    private _builder : FormBuilder,
    private _activatedRoute : ActivatedRoute,
    private _router: Router,
    private _CFService: CentreFormationService) { }

  ngOnInit(): void {
 //   this.fg=this._builder.group({});// pensez a initialiser le formulaire
// console.log(this._activatedRoute.snapshot.params["id"])
    this._service.getOneMbre(this._service.currentUser.idMembre).subscribe(
      (data : Membre) => {
        this.currentMbre = data 
        this.formInit()
      }
    )

    
  }

  formInit()
  {
    /* let Dp=new DatePipe()
    //(navigator.language)
    let DF="yyyy-MM-dd"
    let bday=Dp.transform(this.currentMbre.dateNaiss, DF) */
    console.log(this.currentMbre.dateNaiss)
    console.log(this.currentMbre.dateValiditePermis)
    console.log(this.currentMbre.dateValiditeCF)
    console.log(this.currentMbre.dateFinCF)
    this.fg = this._builder.group({
      firstName : [this.currentMbre.prenom, Validators.required],
      lastName : [this.currentMbre.nom, Validators.required],
      lg : [this.currentMbre.login, Validators.required],
      pw : [this.currentMbre.password, Validators.required],
      cf : [this.currentMbre.centreFormation.idCentre, Validators.required],
      //tranformer en format date en Js 
      birthday : [this.currentMbre.dateNaiss, Validators.required],
      numPermis : [this.currentMbre.numeroPermis, Validators.required],
      datePermis : [this.currentMbre.dateValiditePermis, Validators.required],
      dateValCF : [this.currentMbre.dateValiditeCF, Validators.required],
      dateFinCF : [this.currentMbre.dateFinCF, Validators.required],
      email : [this.currentMbre.email, [Validators.required, Validators.email]]
    })
    this._CFService.GetAllCentreF().subscribe(CFS=>this.centreFormationList=CFS);
  }

  UpdateMbre(){

    const mbreUpdate= new Membre()
    let values = this.fg.value
    //récupérer les info venant du formGroup
    mbreUpdate.prenom= values["firstName"]
    mbreUpdate.nom= values["lastName"]
    mbreUpdate.login= values["lg"]
    mbreUpdate.password= values["pw"]
    mbreUpdate.email= values["email"]
    mbreUpdate.dateNaiss= values["birthday"]
    mbreUpdate.numeroPermis= values["numPermis"]
    mbreUpdate.dateValiditePermis= values["datePermis"]
    mbreUpdate.dateValiditeCF= values["dateValCF"]
    mbreUpdate.dateFinCF= values["dateFinCF"]
    mbreUpdate.centreFormation={"idCentre":values["cf"], "nomCentre":null}
    mbreUpdate.idMembre= this._service.currentUser.idMembre
    this._service.UpdateMbre(mbreUpdate).subscribe({
      next : () => this._router.navigate(["/home"]),
      error : (error) => console.log(error)
    }) 

  }

  changeCF(e) {
    console.log(e.target.value);
  }
}
