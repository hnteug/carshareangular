import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IcentreFormation } from '../../interfaces/icentre-formation';
import { Membre } from '../../models/membre';
import { CentreFormationService } from '../../services/centre-formation.service';
import { MembreService } from '../../services/membre.service';

@Component({
  selector: 'app-membre-create',
  templateUrl: './membre-create.component.html',
  styleUrls: ['./membre-create.component.scss']

})
export class MembreCreateComponent implements OnInit {
  fg : FormGroup
  centreFormationList:IcentreFormation[];
  constructor( private builder: FormBuilder,
    private _service : MembreService,
    private _router: Router,
    private _CFService: CentreFormationService) { }

  ngOnInit(): void {
    this.fg = this.builder.group({
      firstName: ['', Validators.required],
      lastName : ['', Validators.required],
      birthday : ['', Validators.required],
      lg : ['', Validators.required],
      pw : ['', Validators.required],
      cf : ['', Validators.required],
      email : ['', [Validators.required, Validators.email]] 
    })
    this._CFService.GetAllCentreF().subscribe(CFS=>this.centreFormationList=CFS); // ici on appel le service pour precharge la liste de centre de formation 
  }
  InsertMbre() {
    if(this.fg.invalid){
      console.error("Formulaire invalide");
      return;
    }
    const newMbre = new Membre()
    let values = this.fg.value
    //récupérer les info venant du formGroup
    newMbre.prenom= values["firstName"]
    newMbre.nom= values["lastName"]
    newMbre.login= values["lg"]
    newMbre.password= values["pw"]
    newMbre.email= values["email"]
    newMbre.dateNaiss= values["birthday"]
    newMbre.centreFormation={"idCentre":values["cf"], nomCentre:null}
    this._service.InsertMbre(newMbre).subscribe({
      next : () => this._router.navigate(["/home"]),
      error : (error) => console.log(error)
    })    
  }

  changeCF(e) {
    console.log(e.target.value);
  }

}
