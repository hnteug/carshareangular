import { Component, OnInit } from '@angular/core';
import { MembreService } from '../../services/membre.service';
import { Membre } from '../../models/membre';

@Component({
  selector: 'app-membre-list',
  templateUrl: './membre-list.component.html',
  styleUrls: ['./membre-list.component.scss']
})
export class MembreListComponent implements OnInit {
  membreList:Membre [];
  constructor(private mbreservice: MembreService) { }

  ngOnInit(): void {
    this.mbreservice.GetAllMembre().subscribe(mbrList=>{
      console.log(mbrList);
      this.membreList =mbrList;
    })
  }

}
