import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CentreFormation } from 'src/app/models/centre-formation';
import { CentreFormationService } from 'src/app/services/centre-formation.service';

@Component({
  selector: 'app-centre-formation',
  templateUrl: './centre-formation.component.html',
  styleUrls: ['./centre-formation.component.scss']
})
export class CentreFormationComponent implements OnInit {
  fg : FormGroup
  constructor(private builder: FormBuilder,
    private _service : CentreFormationService,
    private _router: Router) { }

  ngOnInit(): void {
    this.fg = this.builder.group({
      centreF: ['', Validators.required]
    })
  }


  InsertCentreF() {
    const newCF = new CentreFormation()
    let values = this.fg.value
    newCF.nomCentre= values["centreF"]
    this._service.InsertCentreF(newCF).subscribe({
      next : () => this._router.navigate(["/home"]),
      error : (error) => console.log(error)
    })}
}
