import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Membre } from 'src/app/models/membre';
import { MembreService } from 'src/app/services/membre.service';
import { Vehicule } from '../../models/vehicule';
import { VehiculeService } from '../../services/vehicule.service';

@Component({
  selector: 'app-vehicule-create',
  templateUrl: './vehicule-create.component.html',
  styleUrls: ['./vehicule-create.component.scss']
})
export class VehiculeCreateComponent implements OnInit {
  fg : FormGroup
  constructor(private builder: FormBuilder,
    private _service : VehiculeService,
    private mbreService: MembreService,
    private _router: Router) { }

  ngOnInit(): void {
//let mbre :Membre = JSON.parse(sessionStorage.getItem('user'))
    this.fg = this.builder.group({
      numPlaque: ['', Validators.required],
      modele : ['', Validators.required],
      marque : ['', Validators.required],
      mbre:[this.mbreService.currentUser.idMembre, Validators.required]
     
    })
    
  }
  InsertVehicule(){
    if(this.fg.invalid){
      console.error("Formulaire invalide");
      return;
    }
    const newVh = new Vehicule()
    let values = this.fg.value
    newVh.marque=values["marque"]
    newVh.modele=values["modele"]
    newVh.numeroPlaque=values["numPlaque"]
    //recuperer le membre qui encode sa voiture
   // newVh.membre={"idMembre":values["mbre"], nom:null}
     newVh.membre=new Membre()
     newVh.membre.idMembre= values["mbre"]
    // naviguer vers l'url proposer un trajet  
 this._service.InsertVh(newVh).subscribe({
      next : () => this._router.navigate(["/home"]),
      error : (error) => console.log(error)
    }) 
  }

}
