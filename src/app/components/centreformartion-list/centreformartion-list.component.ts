import { Component, OnInit } from '@angular/core';
import { IcentreFormation } from '../../interfaces/icentre-formation';
import { CentreFormationService } from '../../services/centre-formation.service';

@Component({
  selector: 'app-centreformartion-list',
  templateUrl: './centreformartion-list.component.html',
  styleUrls: ['./centreformartion-list.component.scss']
})
export class CentreformartionListComponent implements OnInit {

  // centreFormationList: string[];
  centreFormationList:IcentreFormation [];

  constructor(private cfservice: CentreFormationService) { }

  ngOnInit(): void {
     this.cfservice.GetAllCentreF().subscribe(cfList => {
     console.log(cfList);
     this.centreFormationList = cfList;

    
      // this.centreFormationList = cfList as string [];
    })  
  }

}
