import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CentreformartionListComponent } from './centreformartion-list.component';

describe('CentreformartionListComponent', () => {
  let component: CentreformartionListComponent;
  let fixture: ComponentFixture<CentreformartionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CentreformartionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CentreformartionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
